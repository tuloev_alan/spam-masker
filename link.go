package main

import (
	"strings"
)

func main() {

}

// Скрывает ссылки в строке после 'http://'
func HideLink(s string) string {
	var sb strings.Builder
	var hide bool
	for i := 0; i < len(s); i++ {
		if s[i] == ' ' {
			hide = false
		}

		if hide {
			sb.WriteByte('*')
		} else {
			sb.WriteByte(s[i])
			if s[i] == '/' && i-6 >= 0 && s[i-6:i+1] == "http://" {
				hide = true
			}
		}
	}
	return sb.String()
}
