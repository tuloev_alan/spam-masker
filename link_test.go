package main

import "testing"

func Test_HideLink(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "1",
			args: args{
				s: "Here's my spammy page: http://hehefouls.netHAHAHA see you.",
			},
			want: "Here's my spammy page: http://******************* see you.",
		},
		{
			name: "2",
			args: args{
				s: "Here's my spammy page: hTTp://youth-elixir.com",
			},
			want: "Here's my spammy page: hTTp://youth-elixir.com",
		},
		{
			name: "3",
			args: args{
				s: "http:/wdadawd",
			},
			want: "http:/wdadawd",
		},
		{
			name: "4",
			args: args{
				s: "http://hehefouls.netHAHAHA",
			},
			want: "http://*******************",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HideLink(tt.args.s); got != tt.want {
				t.Errorf("HideLink() = %v, want %v", got, tt.want)
			}
		})
	}
}
